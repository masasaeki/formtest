import { BaseForm } from '@/lib'
import { ContactForm } from '@/forms'

describe('ContactForm', () => {
  describe('constructor', () => {
    it('basic', () => {
      const form = new ContactForm()
      expect(form instanceof BaseForm).toBe(true)
    })

    it('has initial values', () => {
      const initialValues = {
        name: 'test name',
        email: 'test@example.com',
        category: 'サービスについて',
        title: 'test title',
        body: 'test body',
      }
      const form = new ContactForm(initialValues)

      expect.assertions(5)
      expect(form.items.name.value).toBe(initialValues.name)
      expect(form.items.email.value).toBe(initialValues.email)
      expect(form.items.category.value).toBe(initialValues.category)
      expect(form.items.title.value).toBe(initialValues.title)
      expect(form.items.body.value).toBe(initialValues.body)
    })
  })

  describe('methods', () => {
    it('buildRequestBody', () => {
      const initialValues = {
        name: 'test name',
        email: 'test@example.com',
        category: 'サービスについて',
        title: 'test title',
        body: 'test body',
      }
      const form = new ContactForm(initialValues)

      const requestBody = form.buildRequestBody()

      expect.assertions(6);
      expect('contact' in requestBody).toBe(true)
      expect('name' in requestBody.contact).toBe(true)
      expect('email' in requestBody.contact).toBe(true)
      expect('category' in requestBody.contact).toBe(true)
      expect('title' in requestBody.contact).toBe(true)
      expect('body' in requestBody.contact).toBe(true)
    })
  })

  describe('validators', () => {
    describe('title, by category', () => {
      it('カテゴリのその他を選択したときタイトルの入力が必要', () => {
        const form = new ContactForm()
        const category = form.items.category
        const otherIndex = category.options.length - 1
        const otherOption = category.options[otherIndex]
        category.value = otherOption.value

        expect.assertions(2)
        expect(form.items.title.invalid).toBe(true)
        form.items.title.value = 'a'
        expect(form.items.title.invalid).toBe(false)
      })

      it('その他以外の選択しているときはタイトルは必須ではない', () => {
        const form = new ContactForm()
        const category = form.items.category
        const firstOption = category.options[0]
        category.value = firstOption.value

        expect.assertions(2)
        expect(form.items.title.invalid).toBe(false)
        form.items.title.value = 'a'
        expect(form.items.title.invalid).toBe(false)
      })
    })
  })
})